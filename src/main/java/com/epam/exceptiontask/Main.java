package com.epam.exceptiontask;

import com.epam.exceptiontask.exceptions.EngineStartException;
import com.epam.exceptiontask.exceptions.EngineStopException;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            Engine engine = new Engine();
            switch (scanner.nextInt()) {
                case 1:
                    engine.startEngine();
                    break;
                case 0:
                    engine.close();
                    break;
            }
        } catch (EngineStartException | EngineStopException e) {
            e.printStackTrace();
        }
    }
}
