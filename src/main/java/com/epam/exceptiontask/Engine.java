package com.epam.exceptiontask;

import com.epam.exceptiontask.exceptions.EngineStartException;
import com.epam.exceptiontask.exceptions.EngineStopException;

public class Engine implements AutoCloseable {

    public void startEngine() throws EngineStartException {
        throw new EngineStartException("Engine can not start.Check the fuel system");
    }

    @Override
    public void close() throws EngineStopException {
        throw new EngineStopException("Engine can not stop.Check the electronics");
    }
}
