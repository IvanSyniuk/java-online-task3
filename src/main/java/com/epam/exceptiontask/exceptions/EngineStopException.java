package com.epam.exceptiontask.exceptions;

import java.io.IOException;

public class EngineStopException extends IOException {

    public EngineStopException(String s) {
        super(s);
    }

}
