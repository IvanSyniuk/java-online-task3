package com.epam.exceptiontask.exceptions;

import java.io.IOException;

public class EngineStartException extends IOException {

    public EngineStartException(String s) {
        super(s);
    }
}
