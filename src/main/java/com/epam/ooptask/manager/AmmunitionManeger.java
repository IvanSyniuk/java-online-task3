package com.epam.ooptask.manager;

import com.epam.ooptask.Choice;
import com.epam.ooptask.ammunition.*;


import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class AmmunitionManeger {
    private List<Ammunition> ammunitions = new ArrayList<>();
    static private Scanner sc = new Scanner(System.in);
    static private AmmunitionManeger ammunitionManeger = new AmmunitionManeger();

    public void showChoice() {
        System.out.println("a:Show all ammunition");
        System.out.println("b:Count price");
        System.out.println("c:Sort by weight and show all");
        System.out.println("d:Add new ammunition");
        System.out.println("e:Stop");
    }

    public void outfit() {
        switch (sc.nextInt()) {
            case 1:
                System.out.println("Helmet");
                ammunitions.add(new Helmet(sc.nextInt(), sc.nextDouble(), sc.next(), sc.next(), sc.next()));
                break;
            case 2:
                System.out.println("Jacket");
                ammunitions.add(new Jacket(sc.nextInt(), sc.nextDouble(), sc.next(), sc.next(), sc.next(), sc.next()));
                break;
            case 3:
                System.out.println("Boots");
                ammunitions.add(new Boots(sc.nextInt(), sc.nextDouble(), sc.next(), sc.next(), sc.next()));
                break;
            case 4:
                System.out.println("Gloves");
                ammunitions.add(new Gloves(sc.nextInt(), sc.nextDouble(), sc.next(), sc.next(), sc.nextInt()));
                break;
        }
        writeToFile();
    }

    public void showAll() {

        for (int i = 0; i < ammunitions.size(); i++) {
            String s1 = ammunitions.get(i).getClass().toString();
            String s2 = ammunitions.get(i).toString();
            String s = new String(new StringBuffer(s1 + " " + s2));
            System.out.println(s.substring(34, s.length()));
        }
    }

    public void countPrice() {
        System.out.println("Price = " + ammunitions.stream().mapToInt(e -> e.getPrice()).sum() + "$");
    }

    public void sortByWeight() {
        ammunitions.sort(Comparator.comparingDouble(Ammunition::getWeight));
        ammunitionManeger.showAll();
    }

    public void writeToFile() {
        try {
            File file = new File("file.txt");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(ammunitions);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getFromFile() {
        try {
            FileInputStream fileInputStream = new FileInputStream("file.txt");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            List<Ammunition> ammunitionsList = (List<Ammunition>) objectInputStream.readObject();

            ammunitions.clear();
            ammunitions.addAll(ammunitionsList);
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ammunitionManeger.getFromFile();
        int stop = 1;
        try {
            while (stop >= 1) {
                ammunitionManeger.showChoice();
                Choice choice = Choice.valueOf(sc.next());
                switch (choice) {
                    case a:
                        ammunitionManeger.showAll();
                        break;
                    case b:
                        ammunitionManeger.countPrice();
                        break;
                    case c:
                        ammunitionManeger.sortByWeight();
                        break;
                    case d:
                        System.out.println("1:helmet\n2:jacket\n3:boots\n4:gloves");
                        ammunitionManeger.outfit();
                        break;
                    case e:
                        stop = 0;
                }
            }
            sc.close();
        } catch (IllegalArgumentException i) {

        }
    }
}
