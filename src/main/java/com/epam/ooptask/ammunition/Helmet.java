package com.epam.ooptask.ammunition;

public class Helmet extends Ammunition {

    private String type;


    public Helmet(int price, double weight, String brand, String model, String type) {
        super(price, weight, brand, model);
        this.type = type;
    }
}
