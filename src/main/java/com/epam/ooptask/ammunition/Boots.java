package com.epam.ooptask.ammunition;

public class Boots extends Ammunition {

    private String type;

    public Boots(int price, double weight, String brand, String model, String type) {
        super(price, weight, brand, model);
        this.type = type;
    }
}
