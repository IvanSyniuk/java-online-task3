package com.epam.ooptask.ammunition;

public class Jacket extends Ammunition {

    private String seasonality;
    private String material;

    public Jacket(int price, double weight, String brand, String model, String seasonality, String material) {
        super(price, weight, brand, model);
        this.seasonality = seasonality;
        this.material = material;
    }
}
