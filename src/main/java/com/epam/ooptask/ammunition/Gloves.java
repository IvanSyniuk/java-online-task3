package com.epam.ooptask.ammunition;

public class Gloves extends Ammunition {

    private int airProtect;

    public Gloves(int price, double weight, String brand, String model, int airProtect) {
        super(price, weight, brand, model);
        this.airProtect = airProtect;
    }
}
