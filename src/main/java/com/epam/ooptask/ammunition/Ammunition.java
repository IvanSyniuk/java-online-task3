package com.epam.ooptask.ammunition;

import java.io.Serializable;

abstract public class Ammunition implements Serializable {

    private static final long serialUID = 1L;
    private int price;
    private double weight;
    private String brand;
    private String model;

    public Ammunition() {

    }

    public Ammunition(int price, double weight, String brand, String model) {
        this.price = price;
        this.weight = weight;
        this.brand = brand;
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Ammunition{" +
                "price=" + price +
                ", weight=" + weight +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}